$(document).ready(function () {
    function header_search() {
        var search_container = $('.search-container'),
            search_icon = $('.search-icon');
        search_icon.click(function () {
            if (search_container.hasClass('active')) {
                search_container.removeClass('active');
            } else {
                search_container.addClass('active');
            }
        })
    }

    header_search();

    function hamburger_menu() {
        var hamburger = $('.hamburger'),
            hamburger_menu = $('.menu-left__container'),
            blackout = $('.blackout')
        hamburger.click(function () {
            if (hamburger.hasClass('active')) {
                hamburger.removeClass('active');
                hamburger_menu.removeClass('active');
                blackout.removeClass('active');
            } else {
                hamburger.addClass('active');
                hamburger_menu.addClass('active');
                blackout.addClass('active');
            }
        })
    }

    hamburger_menu();

    function slick() {
        $('.sidebar-security__items, .partner__items').slick({
            speed: 300,
            autoplay: true,
            autoplaySpeed: 3000,
            slidesToShow: 1,
            infinite: true,
            dots: false,
            prevArrow: '<div class="slick-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></div>',
            nextArrow: '<div class="slick-next"><i class="fa fa-angle-right" aria-hidden="true"></i></div>',
        })
        $('.slider-items').slick({
            speed: 300,
            autoplay: true,
            autoplaySpeed: 3000,
            slidesToShow: 3,
            infinite: true,
            dots: false,
            prevArrow: '<div class="slick-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></div>',
            nextArrow: '<div class="slick-next"><i class="fa fa-angle-right" aria-hidden="true"></i></div>',
            responsive: [
                {
                    breakpoint: 570,
                    settings: {
                        slidesToShow: 2,
                    }
                },
                {
                    breakpoint: 420,
                    settings: {
                        slidesToShow: 1,
                    }
                },
            ]
        })
    }
    slick();

    function category_visible() {
        var category_btn = $('.add-category');
        category_btn.click(function () {
            var $this = $(this),
                $this_parent = $this.closest('.container_bg-none-p'),
                hidden_block = $this_parent.find('.hidden-block'),
                $this_parent_height = $this_parent.outerHeight(),
                hidden_block_items = hidden_block.find('.hidden-block__container'),
                hidden_block_items_height = hidden_block_items.outerHeight();
            $this_parent.css({'height':$this_parent_height + hidden_block_items_height})
            hidden_block.css({'height': hidden_block_items_height, 'overflow': 'auto'});
            console.log($this_parent_height);
        })
    }

    $('.phone').mask('+7(999)999-99-99');
    $('select').select2();

})